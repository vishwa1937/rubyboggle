# RubyBoggle
Boggle game using Ruby, Rails and ReactJS. React JS (16.12.0)  used for front end while for back end Ruby (2.6.5) on Rails (6.0.2) used.

## Installation
For Linux System.

Open up a terminal and navigate to a location where you would like to download this project. Then, enter the given commands sequentially

Clone the project to you local directory using `HTTPS`

```
git clone https://vishwa1937@bitbucket.org/vishwa1937/rubyboggle.git
```

Navigate inside the downloaded project

```
cd RubyBoggle
```

Install all back-end dependencies

```
bundle install
```

Install all front-end dependencies

```
yarn install
```

Migrate db

```
rails db:migrate
```

Run rails server

```
rails s
```

By default, the server tries to run on port 3000. So if you want to run it on a different port:

```
rails s -p <port>
```

Open up your browser and access the app via url `http://localhost:<port>`

## Testing
Unit tests for the controller methods. To run it:

```
rake
```

**Disclaimer**

 https://developer.oxforddictionaries.com Api to validate if a word is valid. 

**Credits and References:**

General game description: `https://en.wikipedia.org/wiki/Boggle`
Example: `https://wordtwist.puzzlebaron.com/init.php`

`https://github.com/ethanwinograd/boggle-ruby`
`https://github.com/mikeheim/boggle`
`https://github.com/eljefe6a/BoggleMapReduce`
`https://github.com/RobAWilkinson/boggle-react`
`https://github.com/Rabin-Lama/ROR-React-boggle`