require 'test_helper'

class BoggleControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test 'should  4x4  array' do
    get :generate_boggle_letters
    json = JSON.parse(response.body)
    assert json['data'].instance_of? Array
    assert json['data'].count == 4
    assert json['data'].flatten.count == 16
  end

  test 'validate word' do
    bl = [
      %w[n a m e],
      %w[b o g g],
      %w[l e u y],
      %w[r u b y]
    ]
    # 'name' should be found in the above board
    post :submit_word, params: { board_letters: bl, word: 'name' }, as: :json
    json = JSON.parse(response.body)
    assert json['result'] == 'name'

    # 'ruby' should be found in the above board
    post :submit_word, params: { board_letters: bl, word: 'ruby' }, as: :json
    json = JSON.parse(response.body)
    assert json['result'] == 'ruby'

    # 'john' should not be found in the above board
    post :submit_word, params: { board_letters: bl, word: 'john' }, as: :json
    json = JSON.parse(response.body)
    assert json['result'] == ''
  end
end
