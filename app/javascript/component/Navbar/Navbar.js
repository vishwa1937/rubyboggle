import React from "react";
import { NavLink } from "react-router-dom";

const Navbar = () => {
  return (
    <div className="navbar-container">
      <NavLink
        className="navbar-link"
        activeClassName="navbar-active"
        to="/boggle"
      >
        Boggle
      </NavLink>
      <NavLink
        className="navbar-link"
        activeClassName="navbar-active"
        to="/rules"
      >
        Rules
      </NavLink>

      <div>
        <img
          className="boggleImage"
          src={require("../../../assets/images/boggle-gif.gif")}
          alt="boggle-gif"
        />
      </div>
    </div>
  );
};
export default Navbar;
