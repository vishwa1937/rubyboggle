import React from "react";
import styled from "styled-components";
import "../../../assets/stylesheets/app.css";
import Navbar from "../Navbar/Navbar";
import Rules from "../Rules/Rules";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

const Button = styled.a`
  display: inline-block;
  text-decoration: none;
  font-weight: bold;
  cursor: pointer;
  color: white !important;
  margin: 20px;
  padding: 5px 20px;
  font-size: 18px;
  border: 2px solid black;
  background: #1d5ccc;
`;

const Boggle = props => {
  return (
    <div>
      <div className="app">
        <Router>
          <Navbar />
          <Switch>
            <Route exact path="/rules" component={Rules} />
            <Button onClick={props.startGame}>Play</Button>
          </Switch>
        </Router>
      </div>
    </div>
  );
};

export default Boggle
