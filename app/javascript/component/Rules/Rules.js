import React from "react";

const Rules = (props) => (
  <div className="rules-wrapper">
    <div className="rules-container">
      <p>Play Boggle to test your lexical skills.</p>
      <p>
        You will have a board filled with letters. The goal is to find words, of
        which letters are digonally, vertically or horizontally placed together
        in a straight line. In the texbox, type the word you think you have
        found in the board and then submit it by hitting return. If the word you
        submitted is found in the board and is actually a word then it will be
        added in the scoreboard, where you will also be able to see all the
        other correct words you have submitted during your gameplay.
      </p>
      <p>
        There is a time limit of 5 minutes, and when the time is up you will not be able to
        submit any more words and will be provided with you total score. The
        score is calculated on the basis of letters' count in the words you
        submit. So the total score is sum of all the words' letters in the
        scoreboard.
      </p>
      <p>
        If you don't like the roll, you can always restart your game. But be
        careful, as your score will also be reset to zero.
      </p>
    </div>
  </div>
);

export default Rules;
