Rails.application.routes.draw do
  get 'validation/index'
  root 'boggle#index'
  get 'get_board_letters', to: 'boggle#generate_boggle_letters'
  post 'submit_word', to: 'boggle#submit_word'
  get 'boggle', action: :index, controller: 'boggle'
  get 'rules', action: :index, controller: 'boggle'
end
